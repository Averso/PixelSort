﻿namespace PixelSort
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonReset = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radioButtonBlack = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonWhite = new System.Windows.Forms.RadioButton();
            this.radioButtonBrightness = new System.Windows.Forms.RadioButton();
            this.numericUpDownLoops = new System.Windows.Forms.NumericUpDown();
            this.labelLoops = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericUpDownWhiteValue = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownBrightnessValue = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownBlackValue = new System.Windows.Forms.NumericUpDown();
            this.labelWhite = new System.Windows.Forms.Label();
            this.labelBrightness = new System.Windows.Forms.Label();
            this.labelBlack = new System.Windows.Forms.Label();
            this.buttonSort = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonHorizontal = new System.Windows.Forms.RadioButton();
            this.radioButtonVertical = new System.Windows.Forms.RadioButton();
            this.toolTipBlack = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipWhite = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipBrightness = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLoops)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWhiteValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrightnessValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlackValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonReset
            // 
            this.buttonReset.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonReset.Location = new System.Drawing.Point(12, 286);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 0;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(633, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(101, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // radioButtonBlack
            // 
            this.radioButtonBlack.AutoSize = true;
            this.radioButtonBlack.Cursor = System.Windows.Forms.Cursors.Default;
            this.radioButtonBlack.Location = new System.Drawing.Point(6, 19);
            this.radioButtonBlack.Name = "radioButtonBlack";
            this.radioButtonBlack.Size = new System.Drawing.Size(51, 17);
            this.radioButtonBlack.TabIndex = 2;
            this.radioButtonBlack.TabStop = true;
            this.radioButtonBlack.Text = "black";
            this.radioButtonBlack.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonWhite);
            this.groupBox1.Controls.Add(this.radioButtonBrightness);
            this.groupBox1.Controls.Add(this.radioButtonBlack);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(88, 100);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sorting mode";
            // 
            // radioButtonWhite
            // 
            this.radioButtonWhite.AutoSize = true;
            this.radioButtonWhite.Cursor = System.Windows.Forms.Cursors.Default;
            this.radioButtonWhite.Location = new System.Drawing.Point(6, 65);
            this.radioButtonWhite.Name = "radioButtonWhite";
            this.radioButtonWhite.Size = new System.Drawing.Size(50, 17);
            this.radioButtonWhite.TabIndex = 5;
            this.radioButtonWhite.TabStop = true;
            this.radioButtonWhite.Text = "white";
            this.radioButtonWhite.UseVisualStyleBackColor = true;
            // 
            // radioButtonBrightness
            // 
            this.radioButtonBrightness.AutoSize = true;
            this.radioButtonBrightness.Cursor = System.Windows.Forms.Cursors.Default;
            this.radioButtonBrightness.Location = new System.Drawing.Point(6, 42);
            this.radioButtonBrightness.Name = "radioButtonBrightness";
            this.radioButtonBrightness.Size = new System.Drawing.Size(73, 17);
            this.radioButtonBrightness.TabIndex = 4;
            this.radioButtonBrightness.TabStop = true;
            this.radioButtonBrightness.Text = "brightness";
            this.radioButtonBrightness.UseVisualStyleBackColor = true;
            // 
            // numericUpDownLoops
            // 
            this.numericUpDownLoops.Cursor = System.Windows.Forms.Cursors.Default;
            this.numericUpDownLoops.Location = new System.Drawing.Point(94, 18);
            this.numericUpDownLoops.Name = "numericUpDownLoops";
            this.numericUpDownLoops.Size = new System.Drawing.Size(98, 20);
            this.numericUpDownLoops.TabIndex = 4;
            this.numericUpDownLoops.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLoops.ValueChanged += new System.EventHandler(this.numericUpDownLoops_ValueChanged);
            // 
            // labelLoops
            // 
            this.labelLoops.AutoSize = true;
            this.labelLoops.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelLoops.Location = new System.Drawing.Point(6, 20);
            this.labelLoops.Name = "labelLoops";
            this.labelLoops.Size = new System.Drawing.Size(32, 13);
            this.labelLoops.TabIndex = 5;
            this.labelLoops.Text = "loops";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericUpDownWhiteValue);
            this.groupBox2.Controls.Add(this.numericUpDownBrightnessValue);
            this.groupBox2.Controls.Add(this.numericUpDownBlackValue);
            this.groupBox2.Controls.Add(this.labelWhite);
            this.groupBox2.Controls.Add(this.labelBrightness);
            this.groupBox2.Controls.Add(this.labelBlack);
            this.groupBox2.Controls.Add(this.labelLoops);
            this.groupBox2.Controls.Add(this.numericUpDownLoops);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox2.Location = new System.Drawing.Point(12, 133);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 127);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Variables";
            // 
            // numericUpDownWhiteValue
            // 
            this.numericUpDownWhiteValue.Cursor = System.Windows.Forms.Cursors.Default;
            this.numericUpDownWhiteValue.Location = new System.Drawing.Point(94, 96);
            this.numericUpDownWhiteValue.Name = "numericUpDownWhiteValue";
            this.numericUpDownWhiteValue.Size = new System.Drawing.Size(98, 20);
            this.numericUpDownWhiteValue.TabIndex = 11;
            this.numericUpDownWhiteValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownWhiteValue.ValueChanged += new System.EventHandler(this.numericUpDownWhiteValue_ValueChanged);
            // 
            // numericUpDownBrightnessValue
            // 
            this.numericUpDownBrightnessValue.Cursor = System.Windows.Forms.Cursors.Default;
            this.numericUpDownBrightnessValue.Location = new System.Drawing.Point(94, 70);
            this.numericUpDownBrightnessValue.Name = "numericUpDownBrightnessValue";
            this.numericUpDownBrightnessValue.Size = new System.Drawing.Size(98, 20);
            this.numericUpDownBrightnessValue.TabIndex = 10;
            this.numericUpDownBrightnessValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBrightnessValue.ValueChanged += new System.EventHandler(this.numericUpDownBrightnessValue_ValueChanged);
            // 
            // numericUpDownBlackValue
            // 
            this.numericUpDownBlackValue.Cursor = System.Windows.Forms.Cursors.Default;
            this.numericUpDownBlackValue.Location = new System.Drawing.Point(94, 44);
            this.numericUpDownBlackValue.Name = "numericUpDownBlackValue";
            this.numericUpDownBlackValue.Size = new System.Drawing.Size(98, 20);
            this.numericUpDownBlackValue.TabIndex = 9;
            this.numericUpDownBlackValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBlackValue.ValueChanged += new System.EventHandler(this.numericUpDownBlackValue_ValueChanged);
            // 
            // labelWhite
            // 
            this.labelWhite.AutoSize = true;
            this.labelWhite.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelWhite.Location = new System.Drawing.Point(6, 98);
            this.labelWhite.Name = "labelWhite";
            this.labelWhite.Size = new System.Drawing.Size(59, 13);
            this.labelWhite.TabIndex = 8;
            this.labelWhite.Text = "whiteValue";
            // 
            // labelBrightness
            // 
            this.labelBrightness.AutoSize = true;
            this.labelBrightness.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelBrightness.Location = new System.Drawing.Point(6, 72);
            this.labelBrightness.Name = "labelBrightness";
            this.labelBrightness.Size = new System.Drawing.Size(82, 13);
            this.labelBrightness.TabIndex = 7;
            this.labelBrightness.Text = "brightnessValue";
            // 
            // labelBlack
            // 
            this.labelBlack.AutoSize = true;
            this.labelBlack.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelBlack.Location = new System.Drawing.Point(6, 46);
            this.labelBlack.Name = "labelBlack";
            this.labelBlack.Size = new System.Drawing.Size(60, 13);
            this.labelBlack.TabIndex = 6;
            this.labelBlack.Text = "blackValue";
            // 
            // buttonSort
            // 
            this.buttonSort.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonSort.Location = new System.Drawing.Point(106, 286);
            this.buttonSort.Name = "buttonSort";
            this.buttonSort.Size = new System.Drawing.Size(75, 23);
            this.buttonSort.TabIndex = 7;
            this.buttonSort.Text = "Sort";
            this.buttonSort.UseVisualStyleBackColor = true;
            this.buttonSort.Click += new System.EventHandler(this.buttonSort_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.Location = new System.Drawing.Point(253, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(359, 282);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonHorizontal);
            this.groupBox3.Controls.Add(this.radioButtonVertical);
            this.groupBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox3.Location = new System.Drawing.Point(116, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(96, 100);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Direction";
            // 
            // radioButtonHorizontal
            // 
            this.radioButtonHorizontal.AutoSize = true;
            this.radioButtonHorizontal.Cursor = System.Windows.Forms.Cursors.Default;
            this.radioButtonHorizontal.Location = new System.Drawing.Point(6, 42);
            this.radioButtonHorizontal.Name = "radioButtonHorizontal";
            this.radioButtonHorizontal.Size = new System.Drawing.Size(70, 17);
            this.radioButtonHorizontal.TabIndex = 4;
            this.radioButtonHorizontal.TabStop = true;
            this.radioButtonHorizontal.Text = "horizontal";
            this.radioButtonHorizontal.UseVisualStyleBackColor = true;
            // 
            // radioButtonVertical
            // 
            this.radioButtonVertical.AutoSize = true;
            this.radioButtonVertical.Cursor = System.Windows.Forms.Cursors.Default;
            this.radioButtonVertical.Location = new System.Drawing.Point(6, 19);
            this.radioButtonVertical.Name = "radioButtonVertical";
            this.radioButtonVertical.Size = new System.Drawing.Size(59, 17);
            this.radioButtonVertical.TabIndex = 2;
            this.radioButtonVertical.TabStop = true;
            this.radioButtonVertical.Text = "vertical";
            this.radioButtonVertical.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 321);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonSort);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pixel Sort";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FormMain_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FormMain_DragEnter);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormMain_MouseDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLoops)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWhiteValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrightnessValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBlackValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.RadioButton radioButtonBlack;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonWhite;
        private System.Windows.Forms.RadioButton radioButtonBrightness;
        private System.Windows.Forms.NumericUpDown numericUpDownLoops;
        private System.Windows.Forms.Label labelLoops;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericUpDownWhiteValue;
        private System.Windows.Forms.NumericUpDown numericUpDownBrightnessValue;
        private System.Windows.Forms.NumericUpDown numericUpDownBlackValue;
        private System.Windows.Forms.Label labelWhite;
        private System.Windows.Forms.Label labelBrightness;
        private System.Windows.Forms.Label labelBlack;
        private System.Windows.Forms.Button buttonSort;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonHorizontal;
        private System.Windows.Forms.RadioButton radioButtonVertical;
        private System.Windows.Forms.ToolTip toolTipBlack;
        private System.Windows.Forms.ToolTip toolTipWhite;
        private System.Windows.Forms.ToolTip toolTipBrightness;
    }
}

