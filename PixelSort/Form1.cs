﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelSort
{
    public partial class FormMain : Form
    {
        private Bitmap inputImage;
        private Bitmap outputImage;
        private Mode mode;
        private bool direction; //true - vertical
        private int blackValue;
        private float brightnessValue;
        private int whiteValue;
        private int loops;
        private string[] validExtensions = new string[] { ".png", ".jpg", ".jpeg", ".bmp"};
        enum Mode
        {
            Black,
            Brightness,
            White
        }
        
        public FormMain()
        {
            InitializeComponent();

            //set up form elements
            this.AllowDrop = true;
            //set new event handler for radio buttons
            radioButtonBlack.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);            
            radioButtonBrightness.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            radioButtonWhite.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            radioButtonVertical.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            radioButtonHorizontal.CheckedChanged += new EventHandler(radioButtons_CheckedChanged);
            //TODO:add handler to direction radio buttons


            //numericUpDowns min/max values
            numericUpDownLoops.Minimum = 1;
            numericUpDownLoops.Maximum = 50;

            numericUpDownBlackValue.Minimum = 0;
            numericUpDownBlackValue.Increment = 65793;
            numericUpDownBlackValue.Maximum = 16777215;

            numericUpDownBrightnessValue.Minimum = 0.0M;
            numericUpDownBrightnessValue.Maximum = 1.0M;
            numericUpDownBrightnessValue.Increment = 0.1M;
            numericUpDownBrightnessValue.DecimalPlaces = 1;

            numericUpDownWhiteValue.Minimum = 0;
            numericUpDownWhiteValue.Increment = 65793;
            numericUpDownWhiteValue.Maximum = 16777215;
                        
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;

            //set tooltips
            toolTipBlack.SetToolTip(numericUpDownBlackValue, "Higher vaules - bigger effect.");
            toolTipBrightness.SetToolTip(numericUpDownBrightnessValue, "Higher vaules - bigger effect.");
            toolTipWhite.SetToolTip(numericUpDownWhiteValue, "Lower vaules - bigger effect.");

            loadDefaultValues();
            //get values from numericUpDowns
            loops = (int) numericUpDownLoops.Value;
            blackValue = (int)numericUpDownBlackValue.Value;
            brightnessValue = (float)numericUpDownBrightnessValue.Value;
            whiteValue = (int)numericUpDownWhiteValue.Value;

            outputImage = null;
            inputImage = null;
            


        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files (*.jpg; *.jpeg; *.bmp; *.png;) | *.jpg; *.jpeg; *.bmp; *.png;";

            DialogResult result = ofd.ShowDialog();

            if (result == DialogResult.OK && ofd.FileName != "")
            {
                inputImage = new Bitmap(ofd.FileName);
            }

            //show new loaded picture in picture box
            pictureBox1.Image = inputImage;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //display save dialog
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Image files (*.jpg; *.jpeg; *.bmp; *.png; *.gif) | *.jpg; *.jpeg; *.bmp; *.png; *.gif";
            DialogResult result = sfd.ShowDialog();

            if (result == DialogResult.OK && sfd.FileName != "")
            {
                outputImage.Save(sfd.FileName);
            }


         
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            loadDefaultValues();
            pictureBox1.Image = inputImage;
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            if(inputImage != null)
                sortPixels();
            else
                MessageBox.Show("Load image first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        
        private void numericUpDownLoops_ValueChanged(object sender, EventArgs e)
        {
            loops = (int)numericUpDownLoops.Value;
        }

        private void numericUpDownBlackValue_ValueChanged(object sender, EventArgs e)
        {
            blackValue = (int)numericUpDownBlackValue.Value;
            
        }

        private void numericUpDownBrightnessValue_ValueChanged(object sender, EventArgs e)
        {
            brightnessValue = (float)numericUpDownBrightnessValue.Value;           
        }

        private void numericUpDownWhiteValue_ValueChanged(object sender, EventArgs e)
        {
            whiteValue = (int)numericUpDownWhiteValue.Value;
          
        }

        private void radioButtons_CheckedChanged (object sender, EventArgs e)
        {
          
            //change enabled option depending on radio buttons state
            numericUpDownBlackValue.Enabled = radioButtonBlack.Checked ? true : false;
            numericUpDownBrightnessValue.Enabled = radioButtonBrightness.Checked ? true : false;
            numericUpDownWhiteValue.Enabled = radioButtonWhite.Checked ? true : false;

            //get mode
            if (radioButtonBlack.Checked)
                mode = Mode.Black;
            else if (radioButtonBrightness.Checked)
                mode = Mode.Brightness;
            else
                mode = Mode.White;

            if (radioButtonVertical.Checked)
                direction = true;
            else
                direction = false;
        }

        //load default values to form's elements
        private void loadDefaultValues()
        {
            radioButtonBlack.Checked = true;
            radioButtonVertical.Checked = true;
            numericUpDownLoops.Value = 1;
            numericUpDownBlackValue.Value = 10000536;//10000000;
            numericUpDownBrightnessValue.Value = 0.6M;
            numericUpDownWhiteValue.Value = 6052956;//6000000;
        }
        
             

        //SORTING PART
        private void sortPixels()
        {
            outputImage = new Bitmap(inputImage);

            //show wait cursor
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();

            if(direction) //vertical - sort by columns             
            {
                for (int row = 0; row < outputImage.Height - 1; row++)
                    sortRow(row);
                for (int column = 0; column < outputImage.Width - 1; column++)
                    sortColumn(column);
            }              
                  
            else  //horizontal - sort by rows    
            {
                for (int column = 0; column < outputImage.Width - 1; column++)
                    sortColumn(column);
                // loop through rows
                for (int row = 0; row < outputImage.Height - 1; row++)
                    sortRow(row);

            }

            //set cursor back to normal and put bitmap to pictureBox
            Cursor.Current = Cursors.Default;          
            pictureBox1.Image = outputImage;
        }


        private void sortRow(int y)
        {                      
            int x = 0;
            int xEnd = 0;

            while (xEnd < outputImage.Width - 1)
            {
                switch (mode)
                {
                    case Mode.Black:
                        x = getFirstNotBlackX(x, y);
                        if (x == outputImage.Width - 1)
                            xEnd = x;
                        else 
                            xEnd = getNextBlackX(x, y);
                        break;
                    case Mode.Brightness:
                        x = getFirstBrightX(x, y);
                        if (x == outputImage.Width - 1)
                            xEnd = x;
                        else
                            xEnd = getNextDarkX(x, y);
                        break;
                    case Mode.White:
                        x = getFirstNotWhiteX(x, y);
                        if (x == outputImage.Width - 1)
                            xEnd = x;
                        else
                            xEnd = getNextWhiteX(x, y);
                        break;
                    default:
                        break;
                }

                if (x < 0) break;

                int sortRange = xEnd - x;

                Color[] pixelSection = new Color[sortRange];

                //get pixels from image 
                for (int i = 0; i < sortRange; i++)
                {
                    pixelSection[i] = outputImage.GetPixel(x + i, y);
                }


                Array.Sort<Color>(pixelSection, CompareColors);

                //save to bitmap
                for (int i = 0; i < sortRange; i++)
                {
                    outputImage.SetPixel(x + i, y, pixelSection[i]);
                }                

                x = xEnd + 1;
            }
        }
        
        private void sortColumn(int x)
        {
            int y = 0;
            int yEnd = 0;

            while(yEnd < outputImage.Height-1 )
            {
                switch(mode)
                {
                    case Mode.Black:
                        y = getFirstNotBlackY(x, y);
                        if (y == outputImage.Height - 1)
                            yEnd = y;
                        else
                            yEnd = getNextBlackY(x, y);
                        break;
                    case Mode.Brightness:
                        y = getFirstBrightY (x, y);
                        if (y == outputImage.Height - 1)
                            yEnd = y;
                        else
                            yEnd = getNextDarkY(x, y);                        
                        break;
                    case Mode.White:
                        y = getFirstNotWhiteY(x, y);
                        if (y == outputImage.Height - 1)
                            yEnd = y;
                        else
                            yEnd = getNextWhiteY(x, y);
                        break;
                    default:
                        break; 

                }

                if (y < 0)
                    break;

                int sortRange = yEnd - y;

                //prepare pixel arrays for pixel section
                Color[] pixelSection = new Color[sortRange];

                //get pixels from image 
                for(int i=0; i<sortRange; i++)
                {
                    pixelSection[i] = outputImage.GetPixel(x, (y + i));
                }

                //sort
                Array.Sort<Color>(pixelSection, CompareColors);

                //save to bitmap
                for (int i = 0; i < sortRange; i++)
                {
                    outputImage.SetPixel(x, (y + i), pixelSection[i]);
                }


                y = yEnd + 1;
                 

            }

        }


        private int CompareColors(Color a, Color b)
        {
            if (a.R < b.R)
                return 1;
            else if (a.R > b.R)
                return -1;
            else
            {
                if (a.G < b.G)
                    return 1;
                else if (a.G > b.G)
                    return -1;
                else
                {
                    if (a.B < b.B)
                        return 1;
                    else if (a.B > b.B)
                        return -1;
                }
            }

            return 0;
        }


        //// BLACK Y ////
        private int getFirstNotBlackY(int x, int y)
        {

            if (y < outputImage.Height)
            {
                int val = outputImage.GetPixel(x, y).ToArgb();

                while (outputImage.GetPixel(x,y).ToArgb() < -blackValue)
                {
                    y++;
                    if (y >= outputImage.Height)
                        return -1;
                }
            }

            return y;
        }

        int getNextBlackY(int x, int y)
        {
            y++;

            if (y < outputImage.Height)
            {
                while (outputImage.GetPixel(x, y).ToArgb() > -blackValue)
                {
                    y++;
                    if (y >= outputImage.Height)
                        return outputImage.Height - 1;
                }
            }

            return y - 1;
        }
        
        //// BLACK X ////
        int getFirstNotBlackX(int x, int y)
        {

            while (outputImage.GetPixel(x, y).ToArgb() < -blackValue)
            {
                x++;
                if (x >= outputImage.Width)
                    return -1;
            }

            return x;
        }

        int getNextBlackX(int x, int y)
        {
            
            x++;

            while (outputImage.GetPixel(x, y).ToArgb() > -blackValue)
            {
                x++;
                if (x >= outputImage.Width)
                    return outputImage.Width - 1;
            }

            return x - 1;
        }

        //// BRIGHTNESS Y ////
        int getFirstBrightY(int x, int y)
        {
            
            if (y < outputImage.Height)
            {
                while (outputImage.GetPixel(x,y).GetBrightness() < brightnessValue)
                {
                    y++;
                    if (y >= outputImage.Height)
                        return -1;
                }
            }

            return y;
        }

        int getNextDarkY(int x, int y)
        {
            y++;

            if (y < outputImage.Height)
            {
                while (outputImage.GetPixel(x, y).GetBrightness() > brightnessValue)
                {
                    y++;
                    if (y >= outputImage.Height)
                        return outputImage.Height - 1;
                }
            }
            return y - 1;
        }


        //// BRIGHTNESS X ////
        int getFirstBrightX(int x, int y)
        {

            while (outputImage.GetPixel(x, y).GetBrightness() < brightnessValue)
            {
                x++;
                if (x >= outputImage.Width)
                    return -1;
            }

            return x;
        }

        int getNextDarkX(int xOrg, int yOrg)
        {
            int x = xOrg + 1;
            int y = yOrg;

            while (outputImage.GetPixel(x, y).GetBrightness() > brightnessValue)
            {
                x++;
                if (x >= outputImage.Width)
                    return outputImage.Width - 1;
            }
            return x - 1;
        }


        //// WHITE Y ////
        int getFirstNotWhiteY(int x, int y)
        {

            if (y < outputImage.Height)
            {
                while (outputImage.GetPixel(x, y).ToArgb() > -whiteValue)
                {
                    y++;
                    if (y >= outputImage.Height)
                        return -1;
                }
            }

            return y;
        }

        int getNextWhiteY(int x, int y)
        {
            y++;

            if (y < outputImage.Height)
            {
                while (outputImage.GetPixel(x, y).ToArgb() < -whiteValue)
                {
                    y++;
                    if (y >= outputImage.Height)
                        return outputImage.Height - 1;
                }
            }

            return y - 1;
        }


        //// WHITE X ////
        int getFirstNotWhiteX(int x, int y)
        {

            while (outputImage.GetPixel(x, y).ToArgb() > -whiteValue)
            {
                x++;
                if (x >= outputImage.Width)
                    return -1;
            }
            return x;
        }

        int getNextWhiteX(int x, int y)
        {
            x++;

            while (outputImage.GetPixel(x, y).ToArgb() < -whiteValue)
            {
                x++;
                if (x >= outputImage.Width)
                    return outputImage.Width - 1;
            }
            return x - 1;
        }

        private void FormMain_MouseDown(object sender, MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Right)
            {
                this.DoDragDrop(inputImage,
                    DragDropEffects.Copy);
            }
        }

        private void FormMain_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) &&
                 (e.AllowedEffect & DragDropEffects.Copy) != 0)
            {
                // Allow this.
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                // Don't allow any other drop.
                e.Effect = DragDropEffects.None;
            }
        }

        private void FormMain_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])(e.Data.GetData(DataFormats.FileDrop, false));
            foreach (string file in files)
            {
                if (validExtensions.Contains(Path.GetExtension(file)))
                {
                    inputImage = new Bitmap(file);
                    pictureBox1.Image = inputImage;
                }

            } 
        }
    }
}
